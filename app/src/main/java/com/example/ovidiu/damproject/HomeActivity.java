package com.example.ovidiu.damproject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ovidiu.damproject.Weather.MainWeather;
import com.example.ovidiu.damproject.models.Users;

public class HomeActivity extends AppCompatActivity {

    Button btnLogout;
    ListView listViewHome;
    String mTitle[]={"Add movie","All movies", "Where all the magic happens?", "Delete my account", "Good weather for a ride to cinema?","Sign out"};
    String mDescription[]={"Add a new movie to the movies you saw","See the movies you have watched","We all want to get there","Delete your account!","Check out the weather to decide to go to the cinema either to watch it online, at home","Log out from your account"};
    int images[]={R.drawable.add100,R.drawable.movie96,R.drawable.favoritefolder100,R.drawable.cinema96,R.drawable.weather100,R.drawable.logoutroundedleft96};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        listViewHome=findViewById(R.id.listViewHome);


        //adapter class
        HomeAdapter adapter= new HomeAdapter(this,mTitle,mDescription,images);
        listViewHome.setAdapter(adapter);


        listViewHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position== 0) {
                    Intent intent= new Intent(getApplicationContext(), AddMovie.class);//   insert app here
                    startActivity(intent);

                } if(position== 1) {
                    Toast.makeText(HomeActivity.this, "See the movies you have watched", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(),SeeMovies.class);
                    startActivity(intent);
                } if(position== 2) {
                    startActivity(new Intent(getApplicationContext(),Maps.class));

                    Toast.makeText(HomeActivity.this, "See your favourite movies", Toast.LENGTH_SHORT).show();
                } if(position== 3) {
                    Toast.makeText(HomeActivity.this, "Account deleted", Toast.LENGTH_SHORT).show();
                    Users u=new Users();
                    u.setId(MainActivity.staticUserId);
                    MainActivity.myDatabase.usersDao().deleteUser(u);
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                } if(position== 4) {
                    Intent intent= new Intent(getApplicationContext(), MainWeather.class);
                    startActivity(intent);
                    Toast.makeText(HomeActivity.this, "See how is the weather to decide", Toast.LENGTH_SHORT).show();
                } if(position== 5){
                    Toast.makeText(HomeActivity.this, "Sign Out",Toast.LENGTH_SHORT).show();

                    SharedPreferences preferences = getSharedPreferences("checkbox", MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("rememberMe","false");
                    editor.apply();

                    finish();

                }
            }
        });

    }



    class HomeAdapter extends ArrayAdapter<String> {
        Context context;
        String rTitle[];
        String rDescription[];
        int rImgs[];

        HomeAdapter(Context c, String title[], String description[], int imgs[]){
            super(c, R.layout.row, R.id.textView_title, title);
            this.context=c;
            this.rTitle=title;
            this.rDescription= description;
            this.rImgs=imgs;

        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.row, parent, false);
            ImageView images = row.findViewById(R.id.imageRow);
            TextView myTitle = row.findViewById(R.id.textView_title);
            TextView myDescription = row.findViewById(R.id.textView_subtitle);

            //setting on views
            images.setImageResource(rImgs[position]);
            myTitle.setText((rTitle[position]));
            myDescription.setText(rDescription[position]);

            return row;
        }
    }
}
