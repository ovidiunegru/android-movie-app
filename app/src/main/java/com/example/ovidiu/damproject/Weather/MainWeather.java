package com.example.ovidiu.damproject.Weather;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.ovidiu.damproject.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainWeather extends AppCompatActivity {

    TextView cityName;
    Button searchButtonWeather;
    TextView result;


    class Weather extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... address) {

            try {
                URL url = new URL(address[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                //establish connection
                connection.connect();

                //retrieve data from url
                InputStream is = connection.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);

                //get data back as a  string
                int data = isr.read();
                String content = "";
                char ch;
                while (data != -1) {
                    ch = (char) data;
                    content = content + ch;
                    data = isr.read();
                }
                return content;


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }


    public void search(View view) {

        cityName = findViewById(R.id.cityname_weather);
        searchButtonWeather = findViewById(R.id.search_btn_weather);
        result = findViewById(R.id.result_weather);

        String cName = cityName.getText().toString();

        String content;
        Weather weather = new Weather();
        try {
            content = weather.execute("https://openweathermap.org/data/2.5/weather?q="
                    + cName + "&appid=b6907d289e10d714a6e88b30761fae22").get();

            Log.i("content", content);


            //JSON
            JSONObject jsonObject = new JSONObject(content);
            String weatherData = jsonObject.getString("weather");

            JSONObject jsonObjectTemp = new JSONObject(content);
            String mainTemperature = jsonObject.getString("main"); //this main is not part of the weather array, it is separately variable like weather
            Log.i("weaterdata", weatherData);


            //weather data is in array

            JSONArray array = new JSONArray(weatherData);
            //JSONArray array1 = new JSONArray(mainTemperature);

            String main = "";
            String description = "";
            String temperature = "";

            for (int i = 0; i < array.length(); i++) {
                JSONObject weatherPart = array.getJSONObject(i);


                main = weatherPart.getString("main");
                description = weatherPart.getString("description");
//                degree=temperaturePart.getString("temp");
            }


            JSONObject mainPart = new JSONObject(mainTemperature);
            temperature = mainPart.getString("temp");

            Log.i("main", main);
            Log.i("description", description);

            //start showing the reuslts on the screen

            String resultText = "Main: " + main + "\nDescription: " + description + "\nTemperature: " + temperature + "degrees Celsius";

            result.setText(resultText);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_weather);


    }
}