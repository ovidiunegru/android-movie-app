package com.example.ovidiu.damproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.ovidiu.damproject.models.Movie;

import java.util.ArrayList;
import java.util.List;

public class MovieList extends AppCompatActivity {

   // private static final String TAG = MovieList.class.getName();

    private static List<Movie> movieList = new ArrayList<>();

    private ListView movieListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);

        Intent intent = getIntent();
        Movie movie = intent.getParcelableExtra("movieKey");
        if (movie != null)
            movieList.add(movie);

        movieListView = findViewById(R.id.lvMovies);

        ArrayAdapter<Movie> arrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, movieList);

        movieListView.setAdapter(arrayAdapter);

        Movie_Adapter mAdapter = new Movie_Adapter(this, movieList);
        movieListView.setAdapter(mAdapter);

    }
}
