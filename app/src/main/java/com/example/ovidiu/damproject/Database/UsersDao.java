package com.example.ovidiu.damproject.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.ovidiu.damproject.models.Users;

import java.util.List;

@Dao
public interface UsersDao {
    @Insert
    public void addUser(Users users);

    @Query("select * from users")
    public List<Users> getUsers();

    @Delete
    public void deleteUser(Users users);

    @Update
    public void updateUser(Users users);
}
