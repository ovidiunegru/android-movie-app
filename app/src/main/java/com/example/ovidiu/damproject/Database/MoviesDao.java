package com.example.ovidiu.damproject.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.ovidiu.damproject.models.Movie;

import java.util.List;

@Dao
public interface MoviesDao {
    @Insert
    public void AddMovie(Movie movie);

    @Query("SELECT * from Movie")
    List<Movie> GetMovies();

    @Delete
    public void DeleteMovie(Movie movie);

    @Update
    public void UpdateMovie(Movie movie);

    @Query("select * from movie where userId=:userdId")
    List<Movie> FindMoviesForMe(final int userdId);
}
