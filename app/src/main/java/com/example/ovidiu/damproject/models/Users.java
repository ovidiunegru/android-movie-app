package com.example.ovidiu.damproject.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class Users {

    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "user_name")
    private String nickname;
    @ColumnInfo(name = "users_password")
    private String userPassword;
    @ColumnInfo(name = "name_users")
    private String nameUsers;
    @ColumnInfo(name = "email")
    private String email;

    public Users(){}

//


    @Ignore
    public Users(String nickname, String userPassword, String nameUsers, String email) {
        this.nickname = nickname;
        this.userPassword = userPassword;
        this.nameUsers = nameUsers;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getNameUsers() {
        return nameUsers;
    }

    public void setNameUsers(String nameUsers) {
        this.nameUsers = nameUsers;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
