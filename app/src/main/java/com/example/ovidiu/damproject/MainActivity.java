package com.example.ovidiu.damproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ovidiu.damproject.Database.MyDatabase;
import com.example.ovidiu.damproject.models.Users;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static MyDatabase myDatabase;
    public static int staticUserId;
    private EditText mTextUsername;
    private EditText mTextPassword;
    private TextView mTextViewRegister;
    private Button mButtonLogin;

    private CheckBox rememberMe;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myDatabase = Room.databaseBuilder(getApplicationContext(),MyDatabase.class,"usersdb").allowMainThreadQueries().build();

        mTextUsername = (EditText) findViewById(R.id.edittext_username);
        mTextPassword = (EditText) findViewById(R.id.edittext_password);
        mTextViewRegister = (TextView) findViewById(R.id.textview_register);
        mButtonLogin = (Button) findViewById(R.id.button_Login);
        rememberMe = (CheckBox) findViewById(R.id.checkbox_rememberMe);

        preferences = getSharedPreferences("checkbox",MODE_PRIVATE);
        String checkbox=preferences.getString("rememberMe","");

        if(checkbox.equals("true")){
            Intent intentCheck= new Intent(this, HomeActivity.class);
            startActivity(intentCheck);
        }else if(checkbox.equals("false")){
            Toast.makeText(this, "Please sign in", Toast.LENGTH_SHORT).show();
        }


        mTextViewRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(registerIntent);
            }
        });

        mButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = mTextUsername.getText().toString().trim();
                String password = mTextPassword.getText().toString().trim();

                List<Users> users= MainActivity.myDatabase.usersDao().getUsers();

                for(Users users1:users){
                    String u=users1.getNickname();
                    String p=users1.getUserPassword();
                    if (u.equals(user) && p.equals(password)){
                        Toast.makeText(MainActivity.this, "Successfully logged in!", Toast.LENGTH_SHORT).show();
                        Intent intentGoToMenu= new Intent(MainActivity.this, HomeActivity.class);
                        staticUserId=users1.getId();
                        startActivity(intentGoToMenu);
                    }else {
                        Toast.makeText(MainActivity.this, "Wrong password!", Toast.LENGTH_SHORT).show();
                    }
                }
                //TODO DAO CHECK FOR LOGIN!
//                Boolean res = db.checkUser(user, password);
//                if (res == true) {
//                    Toast.makeText(MainActivity.this, "Successfully logged in!", Toast.LENGTH_SHORT).show();
//                    Intent intentGoToMenu= new Intent(MainActivity.this, HomeActivity.class);
//                    startActivity(intentGoToMenu);
//                } else {
//                    Toast.makeText(MainActivity.this, "Wrong password!", Toast.LENGTH_SHORT).show();
//
//                }
            }
        });

        rememberMe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    preferences = getSharedPreferences("checkbox", MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("rememberMe","true");
                    editor.apply();

                    Toast.makeText(MainActivity.this, "Checked",Toast.LENGTH_SHORT).show();

                }else if(!buttonView.isChecked()){

                    preferences = getSharedPreferences("checkbox", MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("rememberMe","false");
                    editor.apply();
                    Toast.makeText(MainActivity.this, "Unchecked",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
