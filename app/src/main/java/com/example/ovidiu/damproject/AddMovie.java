package com.example.ovidiu.damproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.example.ovidiu.damproject.models.Movie;

public class AddMovie extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();
    private EditText movieName;
    private EditText movieReleaseDate;
    private Spinner movieGenre;
    private SeekBar movieDuration;

    private RatingBar movieRating;
    private Switch movieRecommendation;
    private Switch isReleased;


    private AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String genre = parent.getItemAtPosition(position).toString();
            Toast.makeText(getApplicationContext(),
                    "Selected value: " + genre, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_movie);

        movieName = findViewById(R.id.etName);
        movieReleaseDate = findViewById(R.id.etReleaseDate);
        movieGenre = findViewById(R.id.spinner);
        movieDuration = findViewById(R.id.seekBar);
        movieRecommendation = findViewById(R.id.switchFavourite);
        movieRating = findViewById(R.id.ratingBar);
        isReleased = findViewById(R.id.switchReleased);


        ArrayAdapter<CharSequence> adapter=ArrayAdapter.createFromResource(this,R.array.movieTypes,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        movieGenre.setAdapter(adapter);
        movieGenre.setOnItemSelectedListener(itemSelectedListener);

        movieDuration.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Log.d(TAG,"Value: " + progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        movieRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Log.d(TAG, "No. of stars: " + rating);
            }
        });

        movieRecommendation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d(TAG, "Value: " + isChecked);
            }
        });
    }

    public void btnSave(View view) {

        Movie movie = new Movie();

        String mName = movieName.getText().toString();
        movie.setMovieName(mName);

        String nDate= movieReleaseDate.getText().toString();
        movie.setMovieReleaseDate(nDate);

        String genre = movieGenre.getSelectedItem().toString();
        movie.setMovieGenre(genre);

        int progress = movieDuration.getProgress();
        movie.setMovieDuration(progress);

        float rating = movieRating.getRating();
        movie.setMovieRating(rating);

        boolean checked = movieRecommendation.isChecked();
        movie.setMovieRecommended(checked);

        boolean isreleased = isReleased.isChecked();
        movie.setReleased(isreleased);

        movie.setUserId(MainActivity.staticUserId);
        MainActivity.myDatabase.moviesDao().AddMovie(movie);

        //send item in the intent for saving it later
        Intent intent = new Intent(getApplicationContext(), MovieList.class);
        intent.putExtra("movieKey", movie);
        startActivity(intent);
    }

}
