package com.example.ovidiu.damproject.Database;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.ovidiu.damproject.models.Movie;
import com.example.ovidiu.damproject.models.Users;

@Database(entities = {Users.class, Movie.class},version =  1,exportSchema = false)
public abstract class MyDatabase extends RoomDatabase {

    public abstract UsersDao usersDao();
    public abstract MoviesDao moviesDao();

}
