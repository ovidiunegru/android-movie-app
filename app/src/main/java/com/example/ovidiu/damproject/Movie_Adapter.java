package com.example.ovidiu.damproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.TextView;

import com.example.ovidiu.damproject.models.Movie;

import java.util.List;

public class Movie_Adapter extends BaseAdapter {


    private Context context;
    private List<Movie> movies;

    public Movie_Adapter(Context context, List<Movie> movies) {
        this.context = context;
        this.movies = movies;
    }

    @Override
    public int getCount() {
        return movies.size();
    }

    @Override
    public Object getItem(int position) {
        return movies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MovieHolder movieHolder = null;
        if(convertView == null)
        {
            convertView = LayoutInflater
                    .from(context)
                    .inflate(R.layout.movie_item,
                            parent,
                            false);
            movieHolder = new MovieHolder(convertView);

        }
        else
        {
            movieHolder = (MovieHolder) convertView.getTag();
        }
        Movie movie = (Movie) getItem(position);
        movieHolder.tvTitle.setText("Movie name: "+movie.getMovieName());
        movieHolder.mvGen.setText("Genre: "+movie.getMovieGenre());
        movieHolder.mvDate.setText("Release date: "+movie.getMovieReleaseDate());
        movieHolder.rbRating.setRating(movie.getMovieRating());
        movieHolder.mvDuration.setText("Movie duration: "+ movie.getMovieDuration().toString());
        movieHolder.swReleased.setChecked(movie.getReleased());
        movieHolder.swRecomended.setChecked(movie.getMovieRecommended());
        return convertView;
    }

    private class MovieHolder
    {
        public TextView tvTitle;

        public TextView mvGen;
        public TextView mvDate;
        public RatingBar rbRating;
        public TextView mvDuration;
        public Switch swReleased;
        public Switch swRecomended;

        MovieHolder(View view)
        {
            tvTitle = view.findViewById(R.id.tviTitle);

            mvGen=view.findViewById(R.id.tvGenre);
            mvDate=view.findViewById(R.id.tvDate);
            rbRating=view.findViewById(R.id.ratingLVM);
            mvDuration=view.findViewById(R.id.durationLVM);
            swReleased=view.findViewById(R.id.switchLVM);
            swRecomended=view.findViewById(R.id.switchRecomended);
        }
    }
}
