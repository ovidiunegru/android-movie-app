package com.example.ovidiu.damproject.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Date;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = Users.class,parentColumns = "id",childColumns = "userId",onDelete = CASCADE))
public class Movie implements Parcelable {

    @ColumnInfo(name = "movie_name")
    private String movieName;
    @ColumnInfo(name = "movie_genre")
    private String movieGenre;
    @ColumnInfo(name = "movie_duration")
    private Integer movieDuration;
    @ColumnInfo(name = "movie_rating")
    private Float movieRating;
    @ColumnInfo(name = "movie_is_recommended")
    private Boolean movieRecommended;
    @ColumnInfo(name = "movie_is_released")
    private Boolean isReleased;
    @ColumnInfo(name = "movie_release_date")
    private String movieReleaseDate; //DateTime picker

    private int userId;
    @PrimaryKey(autoGenerate = true)
    private int id;

    public Movie() {
    }

    @Ignore
    public Movie(String movieName, String movieGenre, Integer movieDuration, Float movieRating, Boolean movieRecommended, Boolean isReleased, String movieReleaseDate, int userId, int id) {
        this.movieName = movieName;
        this.movieGenre = movieGenre;
        this.movieDuration = movieDuration;
        this.movieRating = movieRating;
        this.movieRecommended = movieRecommended;
        this.isReleased = isReleased;
        this.movieReleaseDate = movieReleaseDate;
        this.userId = userId;
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    protected Movie(Parcel in) {

        movieName = in.readString();
        movieGenre = in.readString();
       movieReleaseDate=in.readString();

        if (in.readByte() == 0) {
            movieDuration = null;
        } else {
            movieDuration = in.readInt();
        }
        if (in.readByte() == 0) {
            movieRating = null;
        } else {
            movieRating = in.readFloat();
        }
        byte tmpMovieRecommended = in.readByte();
        movieRecommended = tmpMovieRecommended == 0 ? null : tmpMovieRecommended == 1;

        byte tmpReleaseDate = in.readByte();
        isReleased = tmpReleaseDate == 0 ? null : tmpReleaseDate == 1;

    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getMovieGenre() {
        return movieGenre;
    }

    public void setMovieGenre(String movieGenre) {
        this.movieGenre = movieGenre;
    }

    public Integer getMovieDuration() {
        return movieDuration;
    }

    public void setMovieDuration(Integer movieDuration) {
        this.movieDuration = movieDuration;
    }

    public Float getMovieRating() {
        return movieRating;
    }

    public void setMovieRating(Float movieRating) {
        this.movieRating = movieRating;
    }

    public Boolean getMovieRecommended() {
        return movieRecommended;
    }

    public void setMovieRecommended(Boolean movieRecommended) {
        this.movieRecommended = movieRecommended;
    }

    public Boolean getReleased() {
        return isReleased;
    }

    public void setReleased(Boolean released) {
        isReleased = released;
    }

    public String getMovieReleaseDate() {
        return movieReleaseDate;
    }

    public void setMovieReleaseDate(String movieReleaseDate) {
        this.movieReleaseDate = movieReleaseDate;
    }

    public static Creator<Movie> getCREATOR() {
        return CREATOR;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "movieName='" + movieName + '\'' +
                ", movieGenre='" + movieGenre + '\'' +
                ", movieDuration=" + movieDuration +
                ", movieRating=" + movieRating +
                ", movieRecommended=" + movieRecommended +
                ", isReleased=" + isReleased +
                ", movieReleaseDate='" + movieReleaseDate + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(movieName);
        dest.writeString(movieGenre);
        dest.writeString(movieReleaseDate);
        if (movieDuration == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(movieDuration);
        }
        if (movieRating == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeFloat(movieRating);
        }
        dest.writeByte((byte) (movieRecommended == null ? 0 : movieRecommended ? 1 : 2));
        dest.writeByte((byte) (isReleased == null ? 0: isReleased?1:2));
    }
}
