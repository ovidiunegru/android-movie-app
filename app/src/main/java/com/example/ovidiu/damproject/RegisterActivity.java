package com.example.ovidiu.damproject;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ovidiu.damproject.models.Users;

import java.net.Inet4Address;

public class RegisterActivity extends AppCompatActivity {

    private EditText mTextUsername;
    private EditText mTextPassword;
    private EditText mTextConfirmPassword;
    private EditText mTextName;
    private EditText mTextEmail;
    private TextView mTextViewLogin;
    private Button mButtonRegister;

    private static final String TAG = "MyActivity";

    @SuppressLint("CutPasteId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        //TODO uncomment these!
        mTextUsername =(EditText)findViewById(R.id.edittext_username);
        mTextPassword =(EditText)findViewById(R.id.edittext_password);
        mTextConfirmPassword=(EditText)findViewById(R.id.edittext_confirm_password);
        mTextEmail =(EditText)findViewById(R.id.edittext_email);
        mTextName =(EditText)findViewById(R.id.edittext_name);

        mTextViewLogin=(TextView) findViewById(R.id.textview_login);
        mButtonRegister =(Button) findViewById(R.id.button_Register);


        mTextViewLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent LoginIntent = new Intent(RegisterActivity.this, MainActivity.class);
                startActivity(LoginIntent);
            }
        });

        mButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = mTextUsername.getText().toString().trim();
                String password = mTextPassword.getText().toString().trim();
                String name = mTextName.getText().toString().trim();
                String email = mTextEmail.getText().toString().trim();
                String confirmPass=mTextConfirmPassword.getText().toString().trim();

                Users users= new Users();
                users.setNickname(user);
                users.setUserPassword(password);
                users.setNameUsers(name);
                users.setEmail(email);

                Log.v(TAG, email);


                if(password.equals(confirmPass)){
                    MainActivity.myDatabase.usersDao().addUser(users);
                    Toast.makeText(RegisterActivity.this, "You have registered", Toast.LENGTH_SHORT).show();
                    Intent intent= new Intent(RegisterActivity.this,MainActivity.class);
                    startActivity(intent);
                }else {
                    Toast.makeText(RegisterActivity.this, "The passwords are not matching!", Toast.LENGTH_SHORT).show();
                }


                //TODO DAO ADD USER TO DB
    //         if(password.equals(cnf_password)){
//                    long val = db.addUser(user,password,name,email);
//                    if(val>0){
//                        Toast.makeText(RegisterActivity.this, "You have registered", Toast.LENGTH_SHORT).show();
//                        Intent moveToLogin= new Intent(RegisterActivity.this, MainActivity.class);
//                        startActivity(moveToLogin);
//                    }else{
//                        Toast.makeText(RegisterActivity.this, "Registration error", Toast.LENGTH_SHORT).show();
//
//                    }
//
//
//                }else{
//                    Toast.makeText(RegisterActivity.this, "The passwords are not matching!", Toast.LENGTH_SHORT).show();
//
//                }
            }
        });

    }
}
