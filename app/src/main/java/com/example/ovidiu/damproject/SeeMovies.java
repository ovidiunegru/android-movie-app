package com.example.ovidiu.damproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.ovidiu.damproject.models.Movie;

import java.util.List;

public class SeeMovies extends AppCompatActivity {

    public TextView textViewMovies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_movies);

        textViewMovies = findViewById(R.id.tvShowMovies);

        List<Movie>movieList=MainActivity.myDatabase.moviesDao().FindMoviesForMe(MainActivity.staticUserId);

        String movies ="";
        for(Movie movie :movieList){
            movies += "Movie: " +movie.getMovieName()+"\n Genre: "+movie.getMovieGenre()+"\n\n";
        }

        textViewMovies.setText(movies);
    }
}
